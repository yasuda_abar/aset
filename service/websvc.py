#!/usr/local/bin/python
# -*- coding: utf-8 -*-

##############################################################
# アセットWEBサービス
#

# モジュールインポート
from flask import Flask, render_template, request, redirect, url_for, jsonify
import os
import clustering as cl

# 定数宣言
_ERR_SUCCESS        = 0             # 正常        
_ERR_INVALID_METHOD = -101          # メソッド未対応


#Warningのみログ出力
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# 自身の名称を app という名前でインスタンス化する
app = Flask(__name__)
app.config['DEBUG'] = True
app.config['JSON_AS_ASCII'] = False

#########################################################
# ルーティング処理
#
@app.route('/clustering', methods=['GET','POST'])
def clustering():
    # 戻り値の初期化
    results = {'ErrorCode':_ERR_SUCCESS}

    # サービスメソッド名を取得
    svc_method = ''
    if (request.method == 'POST'):
        svc_method = request.args.POST('method','')
    elif (request.method == 'GET'):
        svc_method = request.args.GET('method','')
    else:
        # 上記以外はメソッドエラー
        results['ErrorCode'] = _ERR_INVALID_METHOD

    # サービスメソッドに応じて処理を振り分ける
    if svc_method == cl.CL_METHOD_SIMILAR_IMAGE_SEARCH:
        # inbox_labelの画像を分類する
        similars = cl.clusteringImage()
        while(len(similars)>0):
            # 処理済みに移動
            cl.updatesimilarimage(similars)
            # 再度類似画像を取得
            results['clastering'] = cl.clusteringImage()
    
    # レスポンスをJSON形式で返す
    return jsonify(results)

#########################################################
# WEBサービス開始
#
if __name__ == '__main__':
    app.run()
#  app.debug = True
#  app.run(host='4d.irodori.world', port = 18000)
