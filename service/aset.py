#!/usr/local/bin/python
# -*- coding: utf-8 -*-

# ライブラリインポート
import os
import json
import logger as log
import clustering as cl

##############################################################
# アセットライブラリ
#


#########################################################
# これ以降、テスト
#
log.trace('>>> test clustering start >>>')
result = cl.clusteringImage()
log.trace(json.dumps(result, indent=4))
log.trace('>>> test clustering end   >>>')
