#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#######################################################
# 教師なし画像分類ライブラリ
#

# クラスインポート定義
import cv2
import os
import shutil
import filelib as fl
import logger as log

#
# 定数
#
CL_METHOD_SIMILAR_IMAGE_SEARCH = 'SimilarImageSearch'       # 類似画像検索
CL_METHOD_ASET = ''

_SIMILARITY_THRESHOLD = 0.8                 # 類似度閾値
_LIMIT_THRESHOLD = 100                      # 特徴量判定の閾値
_DEFAULT_THRESHOLD = 100000                 # 閾値の初期値

_IM_INBOX_FOLDER = './inbox_label'          # 入力ラベルフォルダ
_IM_PROCESS_FOLDER = './process_label'      # 処理済みラベルフォルダ
_IM_ASET_LABEL_FOLDER = './aset_label'      # ASETラベルフォルダ

#########################################################
# 類似画像を検索する
#
def similarImageSearch(srcFile, searchFolder):
    log.trace("=== SimilarImageSearch called ===")
    # 戻り値を初期化
    result_SimilarFiles = []
    # 元画像を200x200[px]にリサイズ
    imgSize = (200,200)
    log.debug(('srcFile = %s/%s' % (searchFolder, srcFile)))
    srcImage = cv2.imread(('%s/%s' % (searchFolder, srcFile)), cv2.IMREAD_GRAYSCALE)
    srcImage = cv2.resize(srcImage, imgSize)
    # 特徴点モデルの設定
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    #detector = cv2.ORB_create()
    detector = cv2.AKAZE_create()
    (target_kp, target_des) = detector.detectAndCompute(srcImage, None)
    # 対象フォルダから類似画像を検索
    targetFiles = os.listdir(searchFolder)
    for chkfile in targetFiles:
        if chkfile == '.DS_Store':
            continue
        elif chkfile == srcFile:
            result_SimilarFiles.append(('%s/%s' % (searchFolder, chkfile)))
            continue
        chkAbsFile = ('%s/%s' % (searchFolder, chkfile))
        result_rate = _DEFAULT_THRESHOLD
        try:
            chkImage = cv2.imread(chkAbsFile, cv2.IMREAD_GRAYSCALE)
            chkImage = cv2.resize(chkImage,imgSize)
            (chk_kp, chk_des) = detector.detectAndCompute(chkImage, None)
            matches = bf.match(target_des, chk_des)
            dist = [m.distance for m in matches]
            result_rate = sum(dist) / len(dist)
            # log.debug(('result: %s rate[%d]' % (chkfile, result_rate)))
        except cv2.error:
            result_rate = _DEFAULT_THRESHOLD
        # 閾値以下なら類似画像配列に追加
        if result_rate < _LIMIT_THRESHOLD:
            # log.debug(('silimars append %s/%s rate[%d]' % (searchFolder, chkfile, result_rate)))
            result_SimilarFiles.append(('%s/%s' % (searchFolder, chkfile)))
    return result_SimilarFiles

#########################################################
# 指定された画像をクラスタリング(分類)する
#
def updatesimilarimage(selectImages):
    log.trace("=== updatesimilarimage called ===")
    # 既存のラベル分類数を取得
    labelcount = fl.GetFolderCount(_IM_ASET_LABEL_FOLDER)
    # 既に既存の分類が存在すれば、既存の分類に属するかをチェック
    labelIndex = labelcount + 1
#    if (labelcount > 0):
#        classifiedFolder = im.match(selectImages[0])
#        if classifiedFolder != '':
#            log.debug(('classified folder:%s' % (classifiedFolder)))
#            labelIndex = int(classifiedFolder)
    labelfolder = ('%07d' % (labelIndex))
    destFolder = ('%s/%s/' % (_IM_ASET_LABEL_FOLDER, labelfolder))
    processFolder = ('%s/%s/' % (_IM_PROCESS_FOLDER, labelfolder))
    # フォルダが存在しなければ作成
    if (os.path.exists(destFolder) != True):
        os.mkdir(destFolder)
    if (os.path.exists(processFolder) != True):
        os.mkdir(processFolder)
    # 選択されたラベル画像を処理
    for selimage in selectImages:
        # ファイル名を取得
        basefile = os.path.basename(selimage)
        # 転送先ファイル名を設定
        destfile = ('%s/%s' % (destFolder, basefile))
        # ASETフォルダにファイルをコピー
        shutil.copy(selimage, destfile)
        log.debug(('copy %s to %s' % (selimage, destfile)))
        # 処理済みファイルを処理済みフォルダに移動
        processFile = ('%s/%s/%s' % (_IM_PROCESS_FOLDER, labelfolder, basefile))
        shutil.move(selimage, processFile)
        log.debug(('move %s to %s' % (selimage, processFile)))
    return

#########################################################
# 画像をクラスタリング(分類)する
#
def clusteringImage():
    # 戻り値を初期化
    result = {}
    # 入力フォルダから対象ファイルリストを取得
    targets = fl.GetJpgFiles(_IM_INBOX_FOLDER)
    similars = similarImageSearch(targets[0], _IM_INBOX_FOLDER)
    while(len(similars)):
        updatesimilarimage(similars)
        # 再度対象ファイルを取得
        targets = fl.GetJpgFiles(_IM_INBOX_FOLDER)
        if (len(targets)>0):
            similars = similarImageSearch(targets[0], _IM_INBOX_FOLDER)
        else:
            similars = []
        
    # 分類結果を設定
    result_clustering = fl.GetFolderList(_IM_PROCESS_FOLDER)
    for clusteringFolder in result_clustering:
        # フォルダ配下のファイル一覧を取得
        targetFolder = '%s/%s' % (_IM_PROCESS_FOLDER, clusteringFolder)
        jpgFiles = fl.GetJpgFiles(targetFolder)
        result[clusteringFolder] = jpgFiles
    
    # 結果を返す
    return result
