#!/usr/local/bin/python
# -*- coding: utf-8 -*-

##################################################################
# ロガー
#

# ライブラリインポート
import inspect
import os
import datetime

# ログレベル
_LOG_LEVEL_ERROR = 0    # エラー
_LOG_LEVEL_WARN = 1     # 警告
_LOG_LEVEL_INFO = 2     # 情報
_LOG_LEVEL_TRACE = 3    # トレース
_LOG_LEVEL_DEBUG = 4    # デバッグ

_LOG_LABEL = ['ERROR', 'WARN', 'INFO', 'TRACE', 'DEBUG']
LOG_LEVEL = _LOG_LEVEL_DEBUG    # 出力するログレベル

# ログフォルダ
_LOG_FOLDER = "./log/"

#############################################################
# エラーレベルのログ出力を行う
# @param msg 出力する文字列
#
def error(msg):
    _logoutput(_LOG_LEVEL_ERROR, msg)

#############################################################
# 警告レベルのログ出力を行う
# @param msg 出力する文字列
#
def warn(msg):
    _logoutput(_LOG_LEVEL_WARN, msg)

#############################################################
# 情報レベルのログ出力を行う
# @param msg 出力する文字列
#
def info(msg):
    _logoutput(_LOG_LEVEL_INFO, msg)

#############################################################
# トレースレベルのログ出力を行う
# @param msg 出力する文字列
#
def trace(msg):
    _logoutput(_LOG_LEVEL_TRACE, msg)

#############################################################
# トレースレベルのログ出力を行う
# @param msg 出力する文字列
#
def debug(msg):
    _logoutput(_LOG_LEVEL_DEBUG, msg)

#############################################################
# ログ出力を行う
# @param logLevel [in] 出力するログレベル
# @param msg [in] 出力する文字列
#
def _logoutput(logLevel,msg):
    # ログレベルの判定
    if (logLevel > LOG_LEVEL):
        return
    # 呼び出し元のファイル名と行番号を取得
    cur = inspect.currentframe()
    info = inspect.getouterframes(cur)[2]
    # ログ文字列を生成
    now = datetime.datetime.now()
    strtime = '%02d:%02d:%02d.%04d' % (now.hour, now.minute, now.second, now.microsecond)
    logmsg = "{stime} [{lglabel}] {msgstr} ({fname},{lno})\n".format(
        stime = strtime, lglabel = _LOG_LABEL[logLevel], msgstr = msg, fname = os.path.basename(info.filename), lno = info.lineno
    )
    # ログフォルダが存在しなければ作成
    if (os.path.exists(_LOG_FOLDER) != True):
        os.makedirs(_LOG_FOLDER)
    # ログファイル名を設定
    logName = _LOG_FOLDER + '%s%s%s.log' % (now.year, now.month, now.day)
    # ログ出力
    with open(logName, mode='a') as fh:
        fh.write(logmsg)