#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################
# Arbarファイルライブラ
#

#
# ライブラリインポート
#
import os
import logger as log

#####################################################
# @brief Jpegファイル一覧取得
#        指定されたフォルダからJPEGファイルを取得する
#
def GetJpgFiles(searchFolder):
    rImages = []
    # 指定されたフォルダのファイル一覧を取得
    files = os.listdir(searchFolder)
    for chkFile in files:
        #拡張子をチェック
        root, ext = os.path.splitext(chkFile)
        if (ext.lower() == ".jpeg" or
            ext.lower() == ".jpg"):
            rImages.append(chkFile)
    return rImages

#####################################################
# @brief 指定されたフォルダ配下のフォルダ数を取得
# @param searchFolder [in] 検索するフォルダ
#
def GetFolderCount(searchFolder):
    files = os.listdir(searchFolder)
    files_dir = [f for f in files if os.path.isdir(os.path.join(searchFolder, f))]
    return len(files_dir)

#####################################################
# @brief 指定されたフォルダ配下のフォルダリストを取得
# @param searchFolder [in] 検索するフォルダ
#
def GetFolderList(searchFolder):
    files = os.listdir(searchFolder)
    files_dir = [f for f in files if os.path.isdir(os.path.join(searchFolder, f))]
    return files_dir
