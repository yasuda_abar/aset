#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################
# Tensorflowを利用したサービスAPI
#

#
# ライブラリインポート
#
import tensorflow as tf
import numpy as np
import scipy.spatial as spatial
import filelib as fl
import operator

#
# デバッグ定数
#
_DEBUG_TRACE = True

#
# 定数
#
_IMAGE_SEARCH_FOLDER = './inbox_label'         #画像検索フォルダ
_TENSORFLOW_CLASSIFY_MODEL = 'classify_image_graph_def.pb'  #分類学習ファイル
_SIMILARITY_THRESHOLD = 0.8                                 #類似度閾値

#################################################
# ログ出力
#
def DebugTrace(msg):
    if _DEBUG_TRACE:
        print(msg)

#################################################
# 類似画像検索
# @param 比較元画像ファイルパス
#
def SimilarImageSearch(srcImgFile = 'kosinokanbai_002.jpg'):
    # 戻り値の初期化
    rImages = []
    # 学習モデルの読込
    with tf.gfile.FastGFile(_TENSORFLOW_CLASSIFY_MODEL, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')
    # 対象ファイル一覧を取得
    images = fl.GetJpgFiles(_IMAGE_SEARCH_FOLDER)
    # TensorFlowのセッションを開始
    features = []
    sel_idx = 0
    idx = 0
    with tf.Session() as sess:
        pool3 = sess.graph.get_tensor_by_name('pool_3:0')
        for img in images:
            image_data = tf.gfile.FastGFile('%s/%s' % (_IMAGE_SEARCH_FOLDER, img), 'rb').read()
            pool3_features = sess.run(pool3,{'DecodeJpeg/contents:0': image_data})
            features.append(pool3_features)
            # 指定された画像のIndexを保存
            if img == srcImgFile:
                sel_idx = idx
            idx = idx + 1
    # 類似度を算出
    query_feat = features[sel_idx]
    sims = [(k, round(1 - spatial.distance.cosine(query_feat, v), 3)) for k,v in enumerate(features)]
    # 類似度の高いものからソート
    results = sorted(sims, key=operator.itemgetter(1), reverse=True)[:len(images) + 1]
    # 類似度が閾値以上のものを抽出
    for item in results:
        if item[1] >= _SIMILARITY_THRESHOLD:
            rImages.append(images[item[0]])
    print('>>>> results start >>>>\n')
    print(results)
    print('<<<< results end   <<<<\n')
    return rImages

#################################################
# これ以降テストメイン
#

# testcase-001
result_images = SimilarImageSearch('kosinokanbai_002.jpg')
print('>>>> testcase-001 start >>>>\n')
print(result_images)
print('<<<< testcase-001 end   <<<<\n')