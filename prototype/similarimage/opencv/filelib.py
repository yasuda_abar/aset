#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################################################
# Arbarファイルライブラ
#

#
# ライブラリインポート
#
import os

#####################################################
# @brief Jpegファイル一覧取得
#        指定されたフォルダからJPEGファイルを取得する
#
def GetJpgFiles(searchFolder):
    rImages = []
    # 指定されたフォルダのファイル一覧を取得
    files = os.listdir(searchFolder)
    for chkFile in files:
        #拡張子をチェック
        root, ext = os.path.splitext(chkFile)
        if (ext.lower() == ".jpeg" or
            ext.lower() == ".jpg"):
            rImages.append(chkFile)
    return rImages