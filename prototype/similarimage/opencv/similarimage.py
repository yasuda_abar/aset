#!/usr/local/bin/python
# -*- coding: utf-8 -*-

################################################
# 類似画像検索クラス
#

# クラスインポート定義
import cv2
import os
import shutil
import filelib as fl

#
# 定数
#
_IMAGE_SEARCH_FOLDER  = './inbox_label/'    # 画像検索フォルダ
_SIMILARITY_THRESHOLD = 0.8                 # 類似度閾値
_LIMIT_THRESHOLD = 100                      # 特徴量判定の閾値
_DEFAULT_THRESHOLD = 100000                 # 閾値の初期値
_PROCESSED_EXT = ".old"                     # 処理済み拡張子

def SimilarImageSearch(srcFile):
    # 戻り値を初期化
    result_SimilarFiles = []
    # 元画像を200x200[px]にリサイズ
    imgSize = (200,200)
    srcImage = cv2.imread(_IMAGE_SEARCH_FOLDER + srcFile,cv2.IMREAD_GRAYSCALE)
    srcImage = cv2.resize(srcImage, imgSize)
    # 特徴点モデルの設定
    bf = cv2.BFMatcher(cv2.NORM_HAMMING)
    #detector = cv2.ORB_create()
    detector = cv2.AKAZE_create()
    (target_kp, target_des) = detector.detectAndCompute(srcImage, None)
    # 対象フォルダから類似画像を検索
    targetFiles = os.listdir(_IMAGE_SEARCH_FOLDER)
    for chkfile in targetFiles:
        if chkfile == '.DS_Store' or chkfile == srcFile:
            continue
        chkAbsFile = _IMAGE_SEARCH_FOLDER + chkfile
        result_rate = _DEFAULT_THRESHOLD
        try:
            chkImage = cv2.imread(chkAbsFile, cv2.IMREAD_GRAYSCALE)
            chkImage = cv2.resize(chkImage,imgSize)
            (chk_kp, chk_des) = detector.detectAndCompute(chkImage, None)
            matches = bf.match(target_des, chk_des)
            dist = [m.distance for m in matches]
            result_rate = sum(dist) / len(dist)
            print(('%s rate[%d]' % (chkfile, result_rate)))
        except cv2.error:
            result_rate = _DEFAULT_THRESHOLD
        # 閾値以下なら類似画像配列に追加
        if result_rate < _LIMIT_THRESHOLD:
            result_SimilarFiles.append(_IMAGE_SEARCH_FOLDER + chkfile)
    return result_SimilarFiles

#################################################
# これ以降テストメイン
#

# testcase-001
result_images = SimilarImageSearch('kosinokanbai_002.jpg')
print('>>>> testcase-001 start >>>>\n')
print(result_images)
print('<<<< testcase-001 end   <<<<\n')