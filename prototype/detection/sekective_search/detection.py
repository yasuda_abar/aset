# -*- coding: utf-8 -*-
import cv2
import selectivesearch

_IMG_FILE = './img/4.jpg'
_IMG_WIDTH = 640
_IMG_HEIGHT = 480

def main():
    # 画像を読み込むでリサイズ
    img = cv2.imread(_IMG_FILE)
    img = cv2.resize(img,(_IMG_WIDTH, _IMG_HEIGHT))

    # perform selective search
    img_lbl, regions = selectivesearch.selective_search(
        img, scale=500, sigma=0.9, min_size=10)

    candidates = set()
    for r in regions:
        # excluding same rectangle (with different segments)
        if r['rect'] in candidates:
            continue
        # excluding regions smaller than 2000 pixels
        if r['size'] < 2000:
            continue
        # distorted rects
        x, y, w, h = r['rect']
        if w / h > 1.2 or h / w > 1.2:
            continue
        candidates.add(r['rect'])

    #画像への枠作成
    for region in candidates:
        x,y,w,h = region
        color = (100, 200, 100)
        cv2.rectangle(img, (x,y), (x+w, y+h), color, thickness=2)
    
    # 結果を保存
    cv2.imwrite('./img/result.jpg',img)

if __name__ == "__main__":
    main()