#!/usr/bin/env python
# -*- coding: utf-8 -*-

from darknetpy.detector import Detector

#detector = Detector('/usr/local/pyenv/versions/3.5.2/lib/python3.5/site-packages/darknetpy',
#                    '/home/users/yasuda/proto/detection/darknet/cfg/coco.names',
#                    '/home/users/yasuda/proto/detection/darknet/cfg/yolo9000.cfg',
#                    '/home/users/yasuda/proto/detection/darknet/cfg/yolo.weights')
detector = Detector('/home/users/yasuda/proto/detection/darknet',
                    '/home/users/yasuda/proto/detection/darknet/cfg/coco.names',
                    '/home/users/yasuda/proto/detection/darknet/cfg/yolo9000.cfg',
                    '/home/users/yasuda/proto/detection/darknet/cfg/yolo.weights')

results = detector.detect('/home/users/yasuda/proto/detection/darknet/img/2.jpg')

print(results)