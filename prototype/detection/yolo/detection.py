#!/usr/bin/env python
# -*- coding: utf-8 -*-
from darkflow.net.build import TFNet
import cv2
 
options = {"model": "cfg/yolo.cfg", "load": "bin/yolo.weights", "threshold": 0.1}
 
tfnet = TFNet(options)
print('\nsuccess to init TFNet.') 
imgcv = cv2.imread("./images/2.jpg")
print('\nsuccess to read image.') 
result = tfnet.return_predict(imgcv)
print('\nsuccess to detect objects.') 
print(result)