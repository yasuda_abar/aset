#!/usr/local/bin/python
# -*- coding: utf-8 -*-

##########################################################
# DropBoxへのアクセスライブラリ
#

# ライブラリのインポート
import dropbox
import os
import logger

###########################################################
# Dropboxのインスタンスを取得する
# @param token [in] 事前に取得したDropBoxのToken
# @return DropBoxのインスタンスを返す。
def createDbx(dbx_token):
    return dropbox.Dropbox(dbx_token)

###########################################################
# Dropboxのファイルリストを取得する
# @param dbx [in] CreateDbxで取得したDropboxのインスタンス
# @param dbx_path [in] ファイル一覧を取得するDropboxのパス。ルートフォルダの場合は空文字を指定
# @return DropBoxのインスタンスを返す。
def ls(dbx, dbx_path):
    return dbx.files_list_folder(dbx_path).entries

###########################################################
# Dropboxからファイルを取得する
# @param dbx [in] CreateDbxで取得したDropboxのインスタンス
# @param dbx_file [in] DropBoxのルートからの相対ファイルパス
# @return テンポラリのダウンロードされたファイル名を返す
def get(dbx, dbx_file):
    # ダウンロードファイル名を生成
    if os.path.exists("work") == False:
        os.mkdir("work")
    downLoadFile = "./work/{0}".format(os.path.basename(dbx_file))
    logger.info("DBX files_download_to_file:{0}".format("/"+dbx_file))
    dbx.files_download_to_file(downLoadFile,"/"+dbx_file)
    return downLoadFile

###########################################################
# Dropboxからファイルを取得する
# @param dbx [in] CreateDbxで取得したDropboxのインスタンス
# @param dbx_file [in] 削除するルートからの相対ファイルパス
def remove(dbx, dbx_file):
    dbx.files_delete("/"+dbx_file)

###########################################################
# Dropboxにファイルを保存する
# @param dbx [in] CreateDbxで取得したDropboxのインスタンス
# @param local_file [in] DropBoxに転送するローカルファイル名
# @param dbx_file [in] DropBoxに保存するルートからの相対ファイル名
def put(dbx, local_file, dbx_file):
    with open(local_file, 'rb') as f:
        dbx.files_upload(f.read(), '/' + dbx_file)