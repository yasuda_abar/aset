#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import dbx
import logger
import os
import shutil
import XML_preprocessor
import pickle
import subprocess

##########################################################
# SSD_Kerasのオブジェクト検知の学習モデル生成モジュール
#

# 定数
# DROPBOXのトークン
DBX_TOKEN = 'Jx3KqdeBdhAAAAAAAAABoT7aWqYvJoIl9ycVN9R0oWHUFFvk4CRzMyAvv-Wk6TTH'
# アノテーションファイルの格納フォルダ
DBX_INFILE_PATH = 'make_annotation/data'
# ローカルデータパス
LOCAL_DATA_PATH = './data'
# 学習フォルダ
TRAIN_DATA_PATH = './train'
# チェックポイントフォルダ
CHECK_POINT_PATH = './checkpoints'

##########################################################
# Dropboxから対象となるファイルを取得
# @return true 正常終了
# @return false エラー発生
def GetTargetfiles():
    logger.info('>>> GetTargetfiles start >>>')
    # ドロップボックスから対象ファイルを保存
    dbxsdk = dbx.createDbx(DBX_TOKEN)
    files = dbx.ls(dbxsdk, '/'+DBX_INFILE_PATH)
    # infileフォルダが無ければ生成
    if (os.path.exists(LOCAL_DATA_PATH) != True):
        os.makedirs(LOCAL_DATA_PATH)
    for f in files:
        targetfile = '%s/%s' % (DBX_INFILE_PATH, os.path.basename(f.name))
        logger.debug(('target file:%s' % (targetfile) ))
        saveFile = dbx.get(dbxsdk, targetfile)
        dstfile = '%s/%s' % (LOCAL_DATA_PATH, os.path.basename(f.name))
        # Toolがバグっているのでここで正しいxmlを出力
        filename, ext = os.path.splitext(f.name)
        if (ext == '.xml'):
            readBuff = ''
            with open(saveFile,'r') as sf:
                readBuff = sf.read()
                readBuff = readBuff.replace('spirce', 'source')
            with open(dstfile, 'w') as wf:
                wf.write(readBuff)
                wf.flush()
            os.remove(saveFile)
        else:
            shutil.move(saveFile, dstfile)
        logger.debug(('Processed %s' % (dstfile)))
    logger.info('<<< GetTargetfiles end   <<<')
    return True

##########################################################
# XMLファイルからPASCALファイルを生成
# @return true 正常終了
# @return false エラー発生
def createPascal():
    logger.info('>>> createPascal start >>>')
    # XMLプロセッサを生成
    data = XML_preprocessor.XML_preprocessor(LOCAL_DATA_PATH+'/').data
    # 学習フォルダが無ければ生成
    if (os.path.exists(TRAIN_DATA_PATH) != True):
        os.makedirs(TRAIN_DATA_PATH)
    pklFile = '%s/train.pkl' % (TRAIN_DATA_PATH)
    pickle.dump(data, open(pklFile, 'wb'))
    logger.info('<<< createPascal end   <<<')
    return True

# Dropboxからファイルを取得
#GetTargetfiles()

# AnnotationファイルからPASCALデータを生成
createPascal()

# 学習タスクを実行
cmd = "python ./ssd_keras/train_ssd_keras.py"
# チェックポイントフォルダが無ければ生成
if (os.path.exists(CHECK_POINT_PATH) != True):
    os.makedirs(CHECK_POINT_PATH)
# 学習タスクを実行
logger.info(('Call %s' % (cmd)))
os.chdir('./ssd_keras')
subprocess.call('python ./train_ssd_keras.py', shell=True)
