// フォルダーリスト配列
var folderLists = {};
// 現在表示中のフォルダ名
var currentFolderName = "";

$(function(){
    // ドラッグ可能なクラスを指定
    $(".selectImage").draggable();
    // ドロップ可能な場所はフォルダ
    $(".folderClass").droppable({
        // 受け入れ可能なクラスはselectImageのみ
        accept : ".selectImage",
        // ドロップされた時はフォルダリストに追加
        drop : function(event, ui){
            // 既存の配列からアイテムを削除
            var imgFileName = ui.draggable.attr("src");
            for(var folderKey in folderLists){
                // 値を取得
                var images = folderLists[folderKey];
                var delIdx = $.inArray(imgFileName, images);
                if (delIdx != -1){
                    folderLists[folderKey].splice(delIdx, 1);
                    break;
                }
            }
            // 現在のフォルダにアイテムを追加
            var targetFolderName = $(this).attr("folderName");
            folderLists[targetFolderName].push(imgFileName);
            // TODO 画面を更新
        }
    });

    /**************************************
     * 画面の再描画
     */
});
